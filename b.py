#!/usr/bin/env python3

import signal
import sys

import tornado.ioloop
import tornado.options

import tcp
import web

io_loop = tornado.ioloop.IOLoop.instance()
tornado.options.define("port", 8000, type=int)


def _term(signal_number=None, stack_frame=None):
    io_loop.stop()


def run():
    signal.signal(signal.SIGTERM, _term)
    tornado.options.parse_command_line()

    port = tornado.options.options.port
    web.make_app().listen(port, xheaders=True)
    tcp.GameServer(max_buffer_size=65536).listen(port + 1)
    tcp.ShellServer(max_buffer_size=1024).listen(port - 1)

    # or
    """
    tcp.GameServer(max_buffer_size=65536).listen(port, socket.gethostbyname(socket.gethostname()))
    web.make_app().listen(port, "127.0.0.1", xheaders=True)
    tcp.ShellServer(max_buffer_size=1024).listen(port, "127.0.0.2")
    """

    sys.stdin = None
    io_loop.start()


if __name__ == "__main__":
    import this
    this
    run()

#!/usr/bin/env python3

import collections
import code
import io
import gc
import os
import sys
import time


def time_int_to_str(i):
    """
    >>> time_int_to_str(0)
    '1970-01-01 08:00:00'
    """
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(i))


def time_str_to_int(s):
    """
    >>> time_str_to_int("1999-9-9 9:9:9")
    936839349
    """
    return int(time.mktime(time.strptime(s, "%Y-%m-%d %H:%M:%S")))


def date_int_to_str(i):
    """
    >>> date_int_to_str(936806400 - 1)
    '1999-09-08'
    """
    return time.strftime("%Y-%m-%d", time.localtime(i))


def date_str_to_int(s):
    """
    >>> date_str_to_int("1999-9-9")
    936806400
    """
    return int(time.mktime(time.strptime(s, "%Y-%m-%d")))


def clock_int_to_str(i):
    """
    >>> clock_int_to_str(86400 - 1)
    '23:59:59'
    """
    return time.strftime("%H:%M:%S", time.gmtime(i))


def clock_str_to_int(s):
    """
    >>> clock_str_to_int("12:00:00")
    43200
    """
    h, m, s = map(int, s.split(":"))
    return 3600 * h + 60 * m + s


def get_logger(logfilename, logsuffix_timefmt="%y%m%d", patch_stderr=False):
    strftime = time.strftime
    timefmt = "%y%m%d%H%M%S"
    assert timefmt.find(logsuffix_timefmt) == 0, logsuffix_timefmt
    logsuffix = strftime(logsuffix_timefmt)
    length = len(logsuffix)
    key = length - 1

    def gen_log_file():
        f = open(logfilename, "a", 1)
        if patch_stderr:
            sys.stderr = f
        return f

    logfile = gen_log_file()

    def rotate_log_file(newsuffix):
        nonlocal logsuffix, logfile
        os.rename(logfilename, "{}.{}".format(logfilename, logsuffix))
        logfile.close()
        logfile = gen_log_file()
        logsuffix = newsuffix

    def log(*args):
        t = strftime(timefmt)
        if t[key] != logsuffix[key]:
            rotate_log_file(t[:length])
        print(t[length:], *args, file=logfile)

    return log


def aggregate_objects(max=30):
    cnt = collections.Counter()
    for o in gc.get_objects():
        cnt[type(o)] += 1

    return [
        ("{}.{}".format(t.__module__, t.__qualname__), n)
        for t, n in cnt.most_common(max)
    ]


class Shell(code.InteractiveInterpreter):
    def __init__(self):
        super().__init__()
        self.buf = []

    def push(self, line):
        buf = self.buf
        buf.append(line.rstrip())
        source = "\n".join(buf)

        more = False
        stdout, stderr = sys.stdout, sys.stderr
        output = sys.stdout = sys.stderr = io.StringIO()
        try:
            more = self.runsource(source)
        finally:
            sys.stdout, sys.stderr = stdout, stderr

        if more:
            return None

        del buf[:]
        return output.getvalue()


if __name__ == "__main__":
    import doctest
    doctest.testmod()

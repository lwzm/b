#!/usr/bin/env python3

import collections


from util import get_logger


log = get_logger("log/player")

PLAYERS = {}
COMMANDS = {}


class Player(dict):

    __slots__ = ["id", "_events", "send"]

    _defaults = {}
    _hooks = {}

    @classmethod
    def register_default(cls, func):
        dct = cls._defaults
        name = func.__name__
        assert name not in dct, name
        dct[name] = func
        return func

    @classmethod
    def register_hook(cls, func):
        dct = cls._hooks
        name = func.__name__
        assert name not in dct, name
        dct[name] = func
        return func

    def __init__(self, source, id):
        self.update(source)
        self.id = id
        self.send = None
        self._events = collections.defaultdict(set)

    def __missing__(self, key):
        value = self._defaults[key](self)
        self[key] = value
        return value

    def __getattr__(self, key):
        return self[key]

    @property
    def is_online(self):
        return callable(self.send)

    def log(self, case, n=1, meta={}):
        log(self.id, case, n, meta)
        for cb, arg in list(self._events[case]):
            self._hooks[cb](arg, self, case, n, meta)

    def on(self, case, hook, arg):
        self._events[case].add((hook, arg))

    def off(self, case, hook, arg):
        self._events[case].discard((hook, arg))


for id in range(100):
    player = Player({}, id)
    PLAYERS[id] = player


def handle(func):
    co = func.__code__
    assert co.co_argcount == 2, (co.co_filename, co.co_firstlineno)
    name = func.__name__
    assert name not in COMMANDS, name
    COMMANDS[name] = func
    return func


if __name__ == "__main__":
    pass

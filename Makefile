
.PHONY: test clean doc install

test:
	#(find . -name "*.py" | xargs pyflakes; true)
	python3 -m unittest test

clean:
	rm -rf build/ dist/ *.egg-info/ 2>/dev/null || true
	find . -name '*.pyc' -delete
	find . -name '*.pyo' -delete
	find . -name 'out' -delete
	find . -type d -name '__pycache__' -delete

doc:
	sphinx-build -b html -d build/docs/doctrees docs build/docs/html

install:
	python3 setup.py install --user

#!/usr/bin/env python3

import queue
import time
import unittest

import util, player, tcp, web


@player.Player.register_default
def gold(_):
    return 5


@player.Player.register_hook
def process_story(task_id, player, case, n, meta):
    pass


@player.Player.register_hook
def daily_tasks(task_id, player, case, n, meta):
    player["gold"] += 1
    player.off(case, "daily_tasks", task_id)


class TestShell(unittest.TestCase):
    def setUp(self):
        self.sh = util.Shell()

    def eq(self, command, result):
        self.assertEqual(self.sh.push(command), result)

    def test_simple(self):
        self.eq("1 + 2", "3\n")
        self.eq("list(range(3))", "[0, 1, 2]\n")

    def test_compound(self):
        self.eq("if True:", None)
        self.eq("    42", None)
        self.eq("    0", None)
        self.eq("", "42\n0\n")

    def test_empty(self):
        self.eq("", "")
        self.eq("", "")
        self.eq("", "")


class TestPlayer(unittest.TestCase):
    def setUp(self):
        self.buffer = []
        self.id = 1
        self.player = player.Player({}, self.id)
        self.player.send = lambda cmd, data: self.buffer.append((self.id, cmd, data))

    def test_dummy_send(self):
        self.player.send(1, 1)
        self.player.send(2, 3)
        self.assertEqual(self.buffer, [(self.id, 1, 1), (self.id, 2, 3)])

    def test_log(self):
        player = self.player
        player.on("wake_up", "process_story", 1)
        player.on("wake_up", "daily_tasks", 1)
        player.on("eat_breakfast", "daily_tasks", 2)
        self.assertEqual(player._events, {
            "wake_up": {("daily_tasks", 1), ("process_story", 1)},
            "eat_breakfast": {("daily_tasks", 2)}
        })
        player.log("wake_up")
        self.assertEqual(player["gold"], 6)
        self.assertEqual(player._events, {
            "wake_up": {("process_story", 1)},
            "eat_breakfast": {("daily_tasks", 2)}
        })


if __name__ == "__main__":
    #print(b.aggregate_objects())
    unittest.main()

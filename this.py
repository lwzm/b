#!/usr/bin/env python3
import collections
import json
import urllib.parse
import sys

import player

@player.handle
def i1(i, msg):
    i.log("wake_up")
    i.send("ok", i.gold)


@player.handle
def i2(i, msg):
    i.on("wake_up", "daily_tasks", 1)


@player.Player.register_default
def gold(_):
    return 5


@player.Player.register_hook
def daily_tasks(task_id, i, key, n, meta):
    print(task_id, i, key, n, meta)

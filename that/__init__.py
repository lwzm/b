#!/usr/bin/env python3

__all__ = ["functions", ]


from . import module_1
from . import module_2


def _init():
    import types

    fns = {}
    base_name = __name__ + "."
    offset = len(base_name)

    def inspect(module):
        for k in dir(module):
            sub = getattr(module, k)
            if isinstance(sub, types.FunctionType):
                module_name = sub.__module__
                if module_name.startswith(base_name):
                    name = "{}.{}".format(module_name[offset:], sub.__name__)
                    fns[name] = sub
                    fns[name.encode()] = sub
            elif isinstance(sub, types.ModuleType):
                module_name = sub.__name__
                if module_name.startswith(base_name):
                    inspect(sub)

    for m in globals().values():
        if isinstance(m, types.ModuleType):
            inspect(m)

    return fns


functions = _init()

#!/usr/bin/env python3

import functools
import json
import traceback
import weakref

import tornado.ioloop
import tornado.iostream
import tornado.options
import tornado.tcpserver
import tornado.gen
import tornado.stack_context

from player import PLAYERS, COMMANDS
from util import Shell, get_logger

shell_log = get_logger("log/shell")
game_log = get_logger("log/game")
json_loads = json.loads
json_dumps = functools.partial(json.dumps, ensure_ascii=False,
                               separators=(",", ":"))


class PlayerConn(object):
    connections = weakref.WeakValueDictionary()

    def __init__(self, stream, address):
        self.stream = stream
        self.address = "{}:{}".format(*address)
        self.stream.read_until(b'\n', self.login)

    def login(self, auth):
        id = int(auth)  # raise if error
        self.proxy = PLAYERS[id]
        if self.proxy.send is not None:
            self.stream.write(b'busy')
            raise Exception(id)
        self.connections[id] = self.stream
        write = self.stream.write

        def send(cmd, data):
            data = json.dumps(data)
            write("{} {}\n".format(cmd, data).encode())
            game_log("<-", cmd, data)

        self.proxy.send = send
        self.stream.set_close_callback(self.logout)
        self.stream.read_until(b'\n', self.msg)

    def msg(self, msg):
        """simple example"""
        cmd, data = msg.decode().split(None, 1)
        game_log("->", cmd, data.rstrip())
        COMMANDS[cmd](self.proxy, json.loads(data))
        self.stream.read_until(b'\n', self.msg)

    def logout(self):
        self.proxy.send = None
        del self.proxy
        self.stream.close()


class GameServer(tornado.tcpserver.TCPServer):
    connections = weakref.WeakValueDictionary()

    def handle_stream1(self, stream, address):
        PlayerConn(stream, address)

    @tornado.gen.coroutine
    def handle_stream(self, stream, address):
        sep = b'\n'
        id = None
        player = None
        try:
            auth = yield stream.read_until(sep)
            id = int(auth)  # raise if error

            if PLAYERS[id].send is not None:
                stream.write(b'busy')
                raise Exception(id)

            def send(cmd, data):
                data = json_dumps(data)
                stream.write((cmd + " " + data + "\n").encode())
                game_log("O", cmd, data)

            self.connections[id] = stream
            player = PLAYERS[id]
            player.send = send
            game_log("login", id, address)

            while True:
                msg = yield stream.read_until(sep)
                cmd, data = msg.decode().split(None, 1)
                game_log("I", cmd, data.rstrip())
                COMMANDS[cmd](player, json_loads(data))

        except tornado.iostream.StreamClosedError:
            game_log("logout", id, address)
        except Exception:
            stream.close()
            game_log("logout", id, address, traceback.format_exc())
        finally:
            if player is not None:
                player.send = None


class ShellServer(tornado.tcpserver.TCPServer):
    shells = weakref.WeakValueDictionary()

    @tornado.gen.coroutine
    def handle_stream(self, stream, address):
        shell_log("shell-login", address)
        self.shells[address] = shell = Shell()
        ps1, ps2 = b'>>> ', b'... '
        try:
            stream.write(ps1)
            while True:
                input = (yield stream.read_until(b'\n')).decode()
                output = shell.push(input)
                shell_log(input, output)
                if output is None:
                    stream.write(ps2)
                else:
                    stream.write(output.encode())
                    stream.write(ps1)
        except tornado.iostream.StreamClosedError:
            shell_log("shell-logout", address)
        except Exception:
            stream.close()
            shell_log(traceback.format_exc())


if __name__ == "__main__":
    GameServer().listen(1024)
    ShellServer().listen(1025)
    tornado.ioloop.IOLoop.instance().start()
